diamonds <- read.csv("Diamonds.csv",
                     header=TRUE,
                     as.is=TRUE)
par(mfrow=c(1,1))

# =====================================
# Problem 1
# =====================================
barplot(table(diamonds$Color))

# =====================================
# Problem 2
# =====================================
# filteredColors  <- data.frame(diamonds[diamonds$Color <= "F",])
# filteredColorsAndClarity  <- data.frame(filteredColors [filteredColors$Clarity == "IF" |
#                                                         filteredColors$Clarity == "VVS1" |
#                                                         filteredColors$Clarity == "VVS2",])
# filteredColors  <- diamonds[diamonds$Color <= "F",]
# filteredColorsAndClarity  <- filteredColors [filteredColors$Clarity == "IF" |
#                                              filteredColors$Clarity == "VVS1" |
#                                              filteredColors$Clarity == "VVS2",]

# a <- table(diamonds$Color,diamonds$Clarity)
# b <- table(filteredColorsAndClarity$Color,filteredColorsAndClarity$Clarity)
# barplot(a, b, beside=TRUE)
# =====================================
# diamonds$Color <- factor(diamonds$Color)
# diamonds$Clarity <- factor(diamonds$Clarity)
# 
# filteredColors <- data.frame(diamonds[diamonds$Color == "D" |
#                                       diamonds$Color == "E" |
#                                       diamonds$Color == "F",])
# 
# filteredColorsAndClarity <- data.frame(filteredColors[filteredColors$Clarity == "IF" |
#                                        filteredColors$Clarity == "VVS1" |
#                                        filteredColors$Clarity == "VVS2",])
# 
# filteredColorsAndClarity$Color <- factor(filteredColorsAndClarity$Color)
# filteredColorsAndClarity$Clarity <- factor(filteredColorsAndClarity$Clarity)
# 
# a <- table(diamonds$Color,diamonds$Clarity)
# b <- table(filteredColorsAndClarity$Color,filteredColorsAndClarity$Clarity)
# barplot(a, b, beside=TRUE)
# =====================================
# colorFactor <- factor(diamonds$Color)
# clarityFactor <- factor(diamonds$Clarity)
# threeColors <- colorFactor[colorFactor=="D" | colorFactor=="E" | colorFactor == "F"]
# threeClarity <- clarityFactor[clarityFactor=="IF" | clarityFactor=="VVS1" | clarityFactor == "VVS2"]
# threeColors <- subset(colorFactor, colorFactor=="D" | colorFactor=="E" | colorFactor=="F")
# threeClarity <- subset(clarityFactor, clarityFactor=="IF" | clarityFactor=="VVS1" | clarityFactor == "VVS2")

# threeColors <- subset(diamonds, Color=="D" | Color=="E" | Color=="F")
# threeInclusions <- subset(diamonds, Clarity=="IF" | Clarity=="VVS1" | Clarity=="VVS2")
# colorFactor <- factor(threeColors$Color)
# clarityFactor <- factor(threeInclusions$Clarity)

# And here is where I would render the information, but I have no idea how to do it
# table
# barplot()

# =====================================
# Problem 3
# =====================================
# Part a
hist(diamonds$Weight)

# Part b
boxplot(diamonds$Weight, ylab="Weight")

# Part c
diamondClarityIF <- diamonds[diamonds$Clarity=="IF",]
diamondClarityVVS1 <- diamonds[diamonds$Clarity=="VVS1",]
diamondClarityVVS2 <- diamonds[diamonds$Clarity=="VVS2",]
diamondClarityVS1 <- diamonds[diamonds$Clarity=="VS1",]
diamondClarityVS2 <- diamonds[diamonds$Clarity=="VS2",]

par(mfrow=c(1,5))
boxplot(diamondClarityIF$Weight, ylab="IF Clarity Weight")
boxplot(diamondClarityVVS1$Weight, ylab="VVS1 Clarity Weight")
boxplot(diamondClarityVVS2$Weight, ylab="VVS2 Clarity Weight")
boxplot(diamondClarityVS1$Weight, ylab="VS1 Clarity Weight")
boxplot(diamondClarityVS2$Weight, ylab="VS2 Clarity Weight")

# Part d
# There needs to be a more elegant solution than this. This is terrible.
globalWeightSummary <- summary(diamonds$Weight)
globalWeightSD <- sd(diamonds$Weight)
ifClarityWeightSummary <- summary(diamondClarityIF$Weight)
ifClarityWeightSD <- sd(diamondClarityIF$Weight)
vvs1ClarityWeightSummary <- summary(diamondClarityVVS1$Weight)
vvs1ClarityWeightSD <- sd(diamondClarityVVS1$Weight)
vvs2ClarityWeightSummary <- summary(diamondClarityVVS2$Weight)
vvs2ClarityWeightSD <- sd(diamondClarityVVS2$Weight)

# barplot.dat <- matrix(c(32,0,107,15),nrow=2)
rowOne <- list(globalWeightSummary[1],
               globalWeightSummary[2],
               globalWeightSummary[3],
               globalWeightSummary[4],
               globalWeightSummary[5],
               globalWeightSummary[6],
               globalWeightSD)
rowTwo <- list(ifClarityWeightSummary[1],
               ifClarityWeightSummary[2],
               ifClarityWeightSummary[3],
               ifClarityWeightSummary[4],
               ifClarityWeightSummary[5],
               ifClarityWeightSummary[6],
               ifClarityWeightSD)
rowThree <- list(vvs1ClarityWeightSummary[1],
                 vvs1ClarityWeightSummary[2],
                 vvs1ClarityWeightSummary[3],
                 vvs1ClarityWeightSummary[4],
                 vvs1ClarityWeightSummary[5],
                 vvs1ClarityWeightSummary[6],
                 vvs1ClarityWeightSD)
rowFour <- list(vvs2ClarityWeightSummary[1],
                vvs2ClarityWeightSummary[2],
                vvs2ClarityWeightSummary[3],
                vvs2ClarityWeightSummary[4],
                vvs2ClarityWeightSummary[5],
                vvs2ClarityWeightSummary[6],
                vvs2ClarityWeightSD)

weightDataFrameRowNames = c("ALL", "IF", "VVS1", "VVS2")
weightDataFrameColumnNames = c("Min", "Q1", "Median", "Mean", "Q3", "Max", "SD")

weightDataFrame <- data.frame(matrix(unlist(rowOne),
                                     nrow=1,
                                     byrow=T))
weightDataFrame <- rbind(weightDataFrame,rowTwo)
weightDataFrame <- rbind(weightDataFrame,rowThree)
weightDataFrame <- rbind(weightDataFrame,rowFour)

rownames(weightDataFrame) <- weightDataFrameRowNames
colnames(weightDataFrame) <- weightDataFrameColumnNames

weightDataFrame

# =====================================
# Problem 4
# =====================================
