Line 123
`(Hep.df$Class <- factor(Hep.df$Class))`

I do not understand what this is doing. I understand that factor is a method to determine the "levels" or rather "categorical" data of an attribute. But where is the information being stored in the `Hep.df$Class`? Is it overwriting the `Class` property with a factor object?

Is this equivalent to saying "for each row's Class attribute change it to a factor object based on the row's original Class value?"

-----

Why does a `factor` object retain the data values, but also contains the "levels" as well?

Even more confusing is that if I enter `typeof(Hep.df$Class)` I receive `integer` as the result instead of a `factor` object after the operation is performed.

Why does `class(Hep.df$Class)` return `factor` and `typeof` doesn't?

-----

Line 134/135
levels(Hep.df$Class)[levels(Hep.df$Class)=="1"] <- "Die" 
levels(Hep.df$Class)[levels(Hep.df$Class)=="2"] <- "Alive"

I understand what this is doing to the data, but I don't understand what this is doing in the underlying language. How do these commands work if they were broken down into a human description of how R is treating these operations? Does this apply some interal iterator logic such that it iterates through each respective element of the list and then performs the operation in the context of the respective iterator?

-----

Line 146
`pct <- round(tab.class/sum(tab.class)*100)`

This makes no sense to me? If I get the "type" of each part I get
```
> class(tab.class)
[1] "table"

```
```
> class(sum(tab.class))
[1] "integer"
```

This applies the division operation across all elements of the array that is in this case the table.

-----

Is there any introduction reading material for R from a programmer's perspective? I'm having a hard time approaching the material as I'm finding the explanations very simplistic and vague with respect to inputs and outputs.


-----
Page 111 - IS Chapter 2 - Section 7 - How to Calculate the Standard Deviation
This section makes a point to explicitly explain that there are two different calculations for the standard deviation: the sample and population standard deviation.
When I was working on the examples in the textbook, I was using R's `sd` function to calculate the standard deviation. When I read the documentation it doesn't indicate if this is calculating based on the sample or population.

Do you know which formula R is using for the `sd` function's calculation?

If they're choosing one formula over another, what are the implications of this in using it as the standard deviation for my calculations?
