Species <- c("Black", "White", "Sumatran", "Javan", "Indian")
Population <- c(3610, 11330, 300, 60, 2500)
barplot(Population, names = Species)

pie(Population)
pie(Population, labels = Names)