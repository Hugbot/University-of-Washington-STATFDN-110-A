readingData <- read.csv("Reading.csv",header=TRUE,as.is=TRUE)

rowlingSummary <- summary(readingData$Rowling)
rowlingSD = sd(readingData$Rowling)

tolstoySummary <- summary(readingData$Tolstoy)
tolstoySD = sd(readingData$Tolstoy)

# ------------------------------------------------------------------------------
bpData <- read.csv("BP.csv",header=TRUE,as.is=TRUE)

dayOneSummary <- summary(bpData$Day1)
dayOneSD = sd(bpData$Day1)

dayTwoSummary <- summary(bpData$Day2)
dayTwoSD = sd(bpData$Day2)