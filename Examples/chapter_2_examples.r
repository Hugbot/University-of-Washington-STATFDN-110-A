exampleOne = quantile(c(0.5, 0.7, 1.1, 1.2, 1.2, 1.3, 1.3, 1.5, 1.5, 1.7, 1.7, 1.8, 1.9, 2.0, 2.2, 2.5, 2.6, 2.8, 2.8, 2.8, 3.5, 3.8, 4.4, 4.8, 4.9, 5.2, 5.5, 5.7, 5.8, 8.0))
stem(exampleOne)

quantile(exampleOne)
boxplot(exampleOne)

# atlantaHawks <- read.csv("atlanta_hawks_wl_seasons.csv",header=TRUE,as.is=TRUE)

numberOfTimesTeenagerIsReminded <- c(0,1,2,3,4,5)
numberOfTimesTeenagerIsRemindedFrequency <- c(2,5,8,14,7,4)
plot(numberOfTimesTeenagerIsReminded, numberOfTimesTeenagerIsRemindedFrequency, type="o")

ageGroup <- c("Children", "Adults", "Retirees")
numberOfPeople <- c(67059,152198,131662)
propertionOfPopulationPct <- c(19, 43, 38)
barplot(propertionOfPopulationPct, names.arg = ageGroup)

maleShoeSizes <- c(9,9,9.5,9.5,10,10,10,10,10,10,10.5,10.5,10.5,10.5,10.5,10.5,10.5,10.5,11,11,11,11,11,11,11,11,11,11,11,11,11,11.5,11.5,11.5,11.5,11.5,11.5,11.5,12,12,12,12,12,12,12,12.5,12.5,12.5,12.5,14)
hist(maleShoeSizes)

# Try It 2.12
# co2emissions <- read.csv("tryit_2-12.csv",header=TRUE,as.is=TRUE)
# the rest is left as an exercise to the reader

# TODO 2.15 Find percentile

# Try It 2.23
exampleTwentyThree <- c(136, 140, 178, 190, 205, 215, 217, 218, 232, 234, 240, 255, 270, 275, 290, 301, 303, 315, 317, 318, 326, 333, 343, 349, 360, 369, 377, 388, 391, 392, 398, 400, 402, 405, 408, 422, 429, 450, 475, 512)
IQR(exampleTwentyThree)
boxplot(exampleTwentyThree)

# Try It 2.28
getMode <- function(x) {
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}
booksCheckedOut <- c(0, 0, 0, 1, 2, 3, 3, 4, 4, 5, 5, 7, 7, 7, 7, 8, 8, 8, 9, 10, 10, 11, 11, 12, 12)
mean(booksCheckedOut)
median(booksCheckedOut)
getMode(booksCheckedOut)

# Try It 2.32
# is `sd` the population or sample standard deviation
baseballTeamAges <- c(21, 21, 22, 23, 24, 24, 25, 25, 28, 29, 29, 31, 32, 33, 33, 34, 35, 36, 36, 36, 36, 38, 38, 38, 40)
baseballTeamAgesMean <- mean(baseballTeamAges)
baseballTeamAgesStandardDeviation <- sd(baseballTeamAges)
valueTwoStandardDeviationsAboveTheMean <- (baseballTeamAgesMean + (baseballTeamAgesStandardDeviation * 2))
