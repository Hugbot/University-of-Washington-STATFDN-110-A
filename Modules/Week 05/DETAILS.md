# Learning Objectives
- Create and interpret relationships in scatterplots.
- Define correlation and calculate a correlation coefficient.
- Properly interpret a correlation coefficient.
- Calculate a simple linear regression equation using least squares regression.
- Interpret the slope and intercept of a regression equation in the context of the problem.
- Identify the assumptions of a simple linear regression model.
- Use R^2 to interpret explained variation in a fitted linear regression model and use se to assess variability of points around a regression line.
- Examine regression residuals to check for violations of assumptions and outliers.
- Make an assessment about outliers, leverage and potential influential data values.
- Notice when you are working with summary data values that make relationships look stronger than they really are.
- Make inference for the slope of a regression line.
- Differentiate between estimation and prediction.
- Use the least squares regression model for estimation and prediction.

# Required Readings
- IS Chapter 12
- IST Chapter 14
