# Required Readings
- IS Chapters 1 and 2
- IST Chapters 1 - 3

Resources for Installing R

Installing R:  https://www.r-project.org/ (Links to an external site.)Links to an external site. ; click on the download R link in the first paragraph and choose the correct version for your operating system
Installing R Studio:  https://www.rstudio.com/ (Links to an external site.)Links to an external site.; choose Download under the RStudio image on the right.
If you are using RStudio, install R first and then RStudio. While R Studio is no required, I highly recommend it. RStudio makes it easier to manage data/projects and to export graphs.
One of many R tutorials to get you started:  https://cran.r-project.org/doc/contrib/Torfs+Brauer-Short-R-Intro.pdf (Links to an external site.)Links to an external site.

Handouts for PowerPoint Lectures that will be covered on Wednesday 11 July.

The handout are pdf files with two slides per page. If you choose to printout any of the materials, consider double-siding your printing if possible.