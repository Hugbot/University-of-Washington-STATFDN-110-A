# Required Readings
- IS Chapters 3, 4.1 – 4.3, 4.6, 5 and 6
- IST Chapters 4 - 6. In Chapter 5 only refer to material for Binomial and Poisson distributions.

Handouts for PowerPoint Lectures that will be covered on Wednesday, 18 July.

The handout are pdf files with two slides per page. If you choose to printout any of the materials, consider double-siding your printing if possible.