# Learning Objectives
- Perform an analysis to look at differences among population means when the number of groups is > 2.
- Learn to compare two competing models and select the best one.
- Understand how to partition total sum of squares into SSGroup and SSError.
- Create an ANOVA table for simple linear regression.
- Combine ANOVA with simple linear regression (ANCOVA).
- Learn to compare five competing models and select the best one, given the data.
- Interpret an ANOVA table with a two-way interaction between a categorical and continuous predictor.

# Required Readings
- IS Chapter 13