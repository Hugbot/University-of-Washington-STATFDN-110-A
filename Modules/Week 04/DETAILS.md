# Learning Objectives
- Distinguish between the two inferential methods of forming confidence intervals and conducting hypothesis tests.
- Outline the steps for conducting a hypothesis test.
- Describe the difference between the significance level of a test and the observed significance level.
- Use either the critical value or P-value approach to make a decision about the outcome of a hypothesis test.
- Make a conclusion in the context of the original question that was being asked.

- Perform large sample hypothesis tests for
    - individual population proportions
    - the difference in population proportions based on two independent samples
- Properly interpret a p-value
- Define Type I , Type II errors and power
- Interpret the consequences of Type I and Type II errors
- Consider trade-offs in sample size and alpha level and the effects on Type I and Type II error rates, and statistical power
- Define effect size for a hypothesis test
- Perform hypothesis tests for individual population means, differences between two population means for both independent samples and matched pairs.
- Distinguish between tests for means with σ known and σ unknown.
- Make a decision to use pooled or unpooled variances for tests for means when working with small samples.
- Make a decision on which test statistic to use for tests for means, z or t.

# Required Readings
- IS Chapters 9 and 10
- IST Chapters 12 and 13
