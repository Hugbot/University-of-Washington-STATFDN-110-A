# Understanding The R Language From a Programmer's Perspective
https://www.datamentor.io/r-programming/

# What is the difference between p(a,b) and p(a|b)?
https://math.stackexchange.com/questions/43623/what-is-the-difference-between-pa-b-and-pab

# Why you need to divide to calc conditional probability 
https://math.stackexchange.com/questions/214739/why-you-need-to-divide-to-calc-conditional-probability

# Conditional Probability Explanation
https://www.mathsisfun.com/data/probability-events-conditional.html